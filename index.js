// const posts = [];

// fetch() method in Javascript is used to request the server and load the information in the webpages. The request can be of any APIs that returns the data of the format JSON

// Syntax: fetch('url', options)
	// url - this is the url to which the request is to be made
	// options - an array of properties. it is an optional parameter
fetch('https://jsonplaceholder.typicode.com/posts').then( (response) => response.json()).then((data) => showPosts(data))

// show posts function
const showPosts = (posts) => {
	// This will take the new post
	let postEntries = '';

	// for each post created, create a new post to be shown
	posts.forEach( (post) => {

		postEntries += `
			<div id="posts-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
			
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
		// console.log(postEntries);
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Add Post

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		
		method: 'POST',
		body: JSON.stringify({

			title: document.querySelector('#txt-title').value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),

		headers: {'Content-Type' : 'application/json'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post successfully added!");

		document.querySelector('#txt-title').value = null;

		document.querySelector('#txt-body').value = null;

		// This will not show due to placeholder is already set
		// showPosts(data)
	})

})

// Edit post function
const editPost = (id) => {


	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	console.log("This is the edit id: " + id);

	document.querySelector("#btn-submit-update").removeAttribute('disabled');

	// Check value of the selected id
	// console.log("txt-edit-ID: " + String(id));
};


// Update Post

document.querySelector('#form-edit-post').addEventListener("submit", (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: 'PUT',
		body: JSON.stringify({
			id: 	document.querySelector('#txt-edit-id').value,
			title: 	document.querySelector('#txt-edit-title').value,
			body: 	document.querySelector('#txt-edit-body').value,
			userId: 1
		}), 
		
		headers: {'Content-Type' : 'application/json'}
	})
	.then((response) => response.json())
	.then((data) => {
		// console.log(data.id);
		console.log(data);
		alert("Post successfully updated");



		document.querySelector('#txt-edit-id').value = null;

		document.querySelector('#txt-edit-title').value = null;

		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);

	})

})

// Delete post function
const deletePost = (id) => {
	console.log(`This is post id to be deleted : ${id}`)
	
	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: 'DELETE',
		body: JSON.stringify({
			id: 	document.querySelector('#txt-edit-id').value,
			title: 	document.querySelector('#txt-edit-title').value,
			body: 	document.querySelector('#txt-edit-body').value,
			userId: 1
		}), 
		
		headers: {'Content-Type' : 'application/json'}
	})
	.then((response) => response.json())
	.then((data) => {
		// console.log(`To test only: ${data}`);
		document.querySelector(`#posts-${id}`).remove();
	})



	// posts = posts.filter((post) => {
	// 	if(post.id.toString() !== id){
	// 		return post;
	// 	}
	// })

	// document.querySelector(`#posts-${id}`).remove();
}